<?php
?>
<div id="comment-<?php print $comment->cid; ?>" class="comment<?php print ($comment->new) ? ' comment-new' : ''; print ' '. $status; print ' '. $zebra; ?>">

<?php if ($picture): ?> 
<div class="comment-avatar">
  <?php print $picture;?> 
</div>
<?php endif; ?>

<div class="comment-body">
  <?php print $content ?>
  <?php if (($submitted) OR ($links)): ?>
    <div class="comment-meta">
      <?php print $submitted ?>
      <?php print l('#', 'node/' . $node->nid, array(
 	      'attributes' => array('title' => t('Permanent Link for this comment')),
 	      'fragment' => 'comment-'.$comment->cid,
            ));
      ?>
      <?php print $links; ?>
    </div>

  <?php endif; ?>
</div>

</div>