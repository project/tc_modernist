﻿<?php
?>
<div id="post-<?php print $node->nid; ?>" class="post <?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">
<div class="post-header">
  <h2><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php if ($submitted): ?><p><?php print $submitted; ?></p><?php endif; ?>
</div>

<?php print $content ?>

<?php if ($page == FALSE): ?>
<?php 
  print l(t('Read the rest of this entry »'), 'node/' . $node->nid, array(
 'attributes' => array('class' => 'more-link'), 
 'fragment' => 'post-comments',
 )); 
?>
<?php endif;?>

<?php if (($node->comment_count > 0) OR ($taxonomy)): ?>
  <div class="post-meta">
  <?php 
    if ($node->comment_count) {
      print l(format_plural($node->comment_count, '1 comment', '@count comments'), "node/" . $node->nid, array(
        'fragment' => 'post-comments',
      ));

    } else {
      print t('Comments off');
    }
    if ($taxonomy) {
      print ' • ' . t('Tagged as: ') . links_for_taxonomy($taxonomy);
    }
  ?>
  </div>
<?php endif;?>


<?php if ($page == TRUE): ?>
<?php 
  $top_links = $node->links;
  // Yoy can try uncoment next 2 line
//  unset($top_links['comment_add']);         // unset add comment
//  unset($top_links['comment_forbidden']);   // unset forbidden comment
  print theme_links($top_links);
?>
<?php endif; ?>
</div>
<?php if (($node->comment_count) AND ($page == TRUE)) { print '<h2 id="post-comments" class="post-comments">' . format_plural($node->comment_count, '1 comment', '@count comments') . '</h2>';}  ?>
