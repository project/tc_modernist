<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <?php print $head ?>
  <title><?php print $head_title ?></title>
  <?php print $styles ?>
  <?php print $scripts ?>
  <!--[if lt IE 8]>
  <link type="text/css" rel="stylesheet" href="<?php print $base_path . $directory; ?>/ie-fixes.css" media="all" />
  <![endif]-->	
<script type="text/javascript" charset="utf-8">
//<![CDATA[
  sfHover = function() {
    var sfEls = document.getElementById("nav").getElementsByTagName("LI");
    for (var i=0; i<sfEls.length; i++) {
      sfEls[i].onmouseover=function() {
      this.className+=" sfhover";
    }
    sfEls[i].onmouseout=function() {
      this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
    }
    }
  }
  if (window.attachEvent) window.attachEvent("onload", sfHover);
//]]>
</script>
</head><body>
<div id="wrapper">	
  <div id="header-wrapper">
    <div id="nav">
        <?php if (isset($primary_links)) : ?>
          <?php print theme('links', $primary_links, array('class' => 'primary-links')) ?>
        <?php endif; ?>
    </div>
    <div id="header">
      <h1><?php print '<a href="'. check_url($front_page) .'" title="'. $site_title .'">'. $site_name . '</a>' ?></h1>
    </div>
  </div>
  <hr />
	
  <div id="content">
    <?php print $breadcrumb; ?>
    <?php print $contentop ?>
      <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
      <?php if ($tabs): print '<div id="tabs" class="clear-block">' . $tabs .'</div>'; endif; ?>
      <?php if ($tabs2): print '<div id="tabs2" class="clear-block">' . $tabs2 .'</div>'; endif; ?>
      <?php if ($show_messages && $messages): print $messages; endif; ?>
      <?php print ($help); ?>
    <?php print $content ?>
    <?php print $contebot; ?>
  </div>
  <hr />

  <div id="sidebar">
    <?php if ($search_box1): print '<div class="section"><h2>' . t('Search') . '</h2>' . $search_box . '</div>'; endif; ?>
    <?php if ($search_box1): ?><?php print $search_box ?><?php endif; ?>
    <?php print $right; ?>
  </div>
  <hr />

  <div id="footer">
    <?php print $footer_message . $footer ?>
  </div>
</div>
<?php print $closure ?>
</body></html>