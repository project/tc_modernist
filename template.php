<?php 
/**
 * Return a themed set of links.
 *
 * @param $links
 *   A keyed array of links to be themed.
 * @param $attributes
 *   A keyed array of attributes
 * @param $as_a
 *   true - return only <a> html array
 * @return
 *   A string containing an unordered list of links.
 */
function phptemplate_links($links, $attributes = array('class' => 'links'), $as_a = FALSE) {
  global $language;
  $output = '';

  if ((count($links) > 0) AND ($as_a == FALSE)) {
    $output = '<ul'. drupal_attributes($attributes) .'>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = '';

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) { $class .= ' first'; }
      if ($i == $num_links) { $class .= ' last'; }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))) { $class .= ' active'; }

      if ($link['attributes']['class'] == 'active-trail') { $class .= ' active'; }

      $output .= '<li>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      else if (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $output .= $link['title'];
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  } elseif ($as_a == TRUE) {
    print '000-'. $as_a . '-111';
    foreach ($links as $key => $link) {
      print $link;
      /*print_r ($links);*/
    }
  }


  return $output;
}

/**
 * Returns the themed submitted-by string for the node.
 */
function phptemplate_node_submitted($node) {
  return t('by !username on !datetime',
    array(
      '!username' => theme('username', $node),
      '!datetime' => format_date($node->created),
//    '!datetime' => date('custum_user_formats',$node->created),
    ));
}

/**
 * Returns the themed submitted-by string for the comment.
 */
function phptemplate_comment_submitted($comment) {
  return t('by !username on !datetime',
    array(
      '!username' => theme('username', $comment),
      '!datetime' => format_date($comment->timestamp)
//    '!datetime' => date('custum_user_formats',$node->created),
    ));
}

/**
 * Returns the themed taxonomy links (only tegs <a>) for the node.
 */
function links_for_taxonomy($taxonomy) {
  $output = '';
  foreach ($taxonomy as $key => $link) {
    $output .= l($link['title'], $link['href'], $link). ', ';
  }
  return rtrim ($output,', ');
}